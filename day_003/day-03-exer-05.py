#!/usr/bin/env python3
# script: love calculator
fname = input("Enter the first person name:\n")
sname = input("Enter second second name:\n")

fname = fname.lower()
sname = sname.lower()

word1 = 'true'
word2 = 'love'
num1 = 0
num2 = 0

for char in word1:
    num1 += fname.count(char)
    num1 += sname.count(char)

for char in word2:
    num2 += fname.count(char)
    num2 += sname.count(char)


score = int(str(num1)+str(num2))


if score < 10 or score > 90:
    print(f"Your score is {score}, you go together like coke and mentos.")
elif score > 40 and score < 50:
    print(f"Your score is {score}, you are alright together.")
else:
    print(f"Your score is {score}.")

