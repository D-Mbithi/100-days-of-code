#!/usr/bin/env python3

height = float(input("Please your height(cm): "))

weight = float(input("Please enter you weight(kg): "))

height = height / 100

bmi = round(weight / height**2)

if bmi < 18.5:
    print(f"Your bmi is{bmi},Your are underweight")
elif bmi < 25:
    print(f'Your bmi is {bmi}, You are normal weight')
elif bmi < 30:
    print(f'Your bmi is {bmi}, You are slightly overweight')
elif bmi < 35:
    print(f'Your bmi is {bmi}, You are obese')
else:
    print(f'Your bmi is {bmi}, You are clinically obese')
