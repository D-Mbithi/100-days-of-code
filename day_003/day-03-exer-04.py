#!/usr/bin/env python3
# Script: pizza order solution
#

# pizza sizes cost
small = 15
medium = 20
large = 25

# toping cost
pepperoni_small = 2
pepperoni_large_medium = 3

extra_cheese = 1

print("Welcome to pizza order service.")
print("-------------------------------\n")

print("Select pizza (S)mall, (M)edium, (L)arge:")
size = input("Size(S,M,L):")

print("Add pepperoni in pizza?")
topping = input("(Y/N):")

print("Add extra cheese?")

cheese = input("(Y/N):")

total_cost = 0

if size.capitalize() == 'S':
    total_cost += small
elif size.capitalize() == 'M':
    total_cost += medium
elif size.capitalize() == 'L':
    total_cost += large

print(total_cost)

if topping.capitalize() == 'Y':
    if size.capitalize == 's':
        total_cost += pepperoni_small
    else:
        total_cost += pepperoni_large_medium

if cheese.capitalize() == 'Y':
    total_cost += extra_cheese

print(f"Your final bill is: ${total_cost}")
