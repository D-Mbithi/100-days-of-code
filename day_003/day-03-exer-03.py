#!/usr/bin/env python3
# Script: leap year solution
#

year = int(input("Enter year to check for leap year.\n"))

if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print(f"{year} is a leap year.")
        else:
            print(f"{year} is not a leap year")
    else:
        print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")
