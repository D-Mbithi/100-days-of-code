# 100 Days of Code.

## Day 01
Implemented a script to generate band names with input from users. Capture user input to variables city and pet_name. Print the band name to console.

## Day 02
Implemented a script to calculate bill for a group and include tip for the waiter. The script takes three inputs: bill, percentage for tip, number of people in the group. It return the total bill for each person.

Also today I learned that, numerical value which are represented by including commas for readerability for humans can be represented using underscore in the following format in python.

```
123,456,789

```

```
123_456_789i

```

Day 2 Exer 1: Created script to calculate sum of two digits from user input of a number with 2 digits. Split the input into two digits the add the two digits.

Day 2 Exer 2: Created a script to calculate the BMI with user input of weight in kgs and height in meters.

## Day 003
Exercise solution programs implemented:
- Love calculater
- BMI Advanced calculater
- Pizza order
- Odd/Even checker
- Leap year checker

I have learned impletementing loop and nested logic in python.

## Day 004
Implemented Rock, paper scissors game.
