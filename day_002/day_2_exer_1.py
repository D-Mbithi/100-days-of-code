#!/usr/bin/env python3
# Script: day_2_exer_1.py
# Adds two digits number from user
# 39 => 3 + 9 = 12

print("Enter a two digit number: ")
numbers = input(">")

numbers = list(numbers)

num1 = int(numbers[0])
num2 = int(numbers[1])

num_sum = num1 + num2

print(num_sum)
