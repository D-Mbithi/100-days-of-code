#!/usr/bin/env python3
# Script: day_2_exer_2.py
# script calculates the Body Mass Index (BMI)

# Get user input
print("Please enter your weight in Kg:")
weight = float(input(">"))

print("Please enter your height in Meters:")
height = float(input(">"))

bmi = weight / (height * height)

print(int(bmi))
