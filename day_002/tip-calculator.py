#!/usr/bin/env python3
# Script: tip-calculator.py
# Calculate cost and tip to be paid

welcome = "Welcom to tip calculator"

print(welcome)
print('-'*len(welcome))
print()

# capture user information
print("What was the total bill?")
bill = float(input("> "))
print("How many people to split the bill?")
people = int(input("> "))
print("What percentage tip would you like to give(10,12,15)?")
percentage = float(input(">"))

total_bill = bill + (bill * percentage/100)

total_per_person = round(total_bill / people, 1)

print(f"Each person should pay ${total_per_person}.")
