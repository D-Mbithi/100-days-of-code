#!/usr/bin/env python3
# Day 01
# Script: band name generator

message = "Welcome to band generator program."

print(message)
print('-'*len(message))

print()
print("What's the name of the city your grew up in?")
city = input('> ')

print()
print("What's the name of your pet?")
pet = input('> ')

print()
print(f"Your band name is {city} {pet}.")
