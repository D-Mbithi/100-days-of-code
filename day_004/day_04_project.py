import random

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

# Rules
"""
Rock wins against scissors.

Scissors win against paper.

Paper wins against rock.
"""

# Write your code below this line 👇
print(rock)
print(paper)
print(scissors)

play = True

while play:

    guest_play = input("Enter you choice[rock,paper,scissors]:\n")
    if guest_play.lower not in ['rock', 'paper', 'scissors']:
        print("Enter correct choice.")

    computer_play = random.choice(['paper', 'scissors', 'rock'])

    print(f"Guest: {guest_play}")
    print(f"Computer: {computer_play}")

    if guest_play == 'rock' and computer_play == 'scissors':
        print('Guest wins, computer losses.')
    elif guest_play == 'scissors' and computer_play == 'paper':
        print("Guest wins and computer losses.")
    elif guest_play == 'paper' and computer_play == 'rock':
        print('Guest wins and computer losses.')
    elif guest_play == computer_play:
        print("It's a draw.")
    else:
        print("Computer wins, and guest losses.")

    game_continue = input("\n\nPlay again?[yn]")

if game_continue != 'y':
    play = False
    print('Goodbye....')
